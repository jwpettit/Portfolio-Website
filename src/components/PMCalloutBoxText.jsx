import BlockContent from '@sanity/block-content-to-react'

export default function CalloutBox({ blocks }) {
  return (
    <>
      <BlockContent blocks={blocks} />
    </>
  );
}
