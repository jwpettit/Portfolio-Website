import StyledIconButton from "./CustomIconButton";

export default function ContactGitLabIcon(props) {
    return (
        <StyledIconButton
            href={props.props.contactPageData.gitLabLink}
            target="blank"
        >
            <img
                src={`${process.env.PUBLIC_URL}/gitlab-logo-600.svg`}
                height="45"
            />
        </StyledIconButton>
    );
}
